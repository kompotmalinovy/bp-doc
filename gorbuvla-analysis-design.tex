\chap Analysis and Design

This chapter covers demands placed on application, analysis and design decisions which 
had to be made before implementation. Some common trends in iOS development are investigated, best practices and 
alternative approaches are examined before final decision whether specific technology, framework or architectural 
approach will be used in final project. Some decisions were influenced by prior experience with Android development.


\sec	Programming Language

There is a variety of languages available when it comes to iOS development. The most popular ones have proven to be 
ReactNative from Facebook, Xamarin from Microsoft, Swift and Objective-C from Apple. I am still very skeptical when it 
comes to cross-platform development, mainly due to maintenance, and, given the requirement to collect contextual device 
data, thus using system frameworks extensively, I have made a choice to go with native approach, meaning I had to choose 
either Swift or Objective-C. Objective-C is rather old language, based on C and Smalltalk. Its vague syntax made me quickly 
turn my attention to other, more modern alternative. Swift, unlike Objective-C, is a modern, multi-paradigm language 
developed with existing Apple frameworks compatibility in mind.  It immediately seemed like a good candidate, given its 
popularity\urlnote{http://redmonk.com/sogrady/2018/03/07/language-rankings-1-18/} and many attractive features.

The following list stresses the most significant ones:
\begitems

* Protocol oriented. In most modern languages protocols are known as interfaces. One of the Swift's protocols most powerful
features is protocol extension, which allows existing third party classes to conform to user-defined protocols.

* Powerful enums and pattern matching. This feature is especially handy when using the Result Pattern\urlnote{https://
fsharpforfunandprofit.com/posts/recipe-part2/}. A user-defined enum with associated values can represent a stateful 
operation state, using pattern matching we then match the returned value against predefined conditions.

* Swift introduces an important concept of optional types, referred as Null Safety in other languages. This type-system 
feature aims to eliminate null-pointer errors by Optional enumerations which can be either ".none" or ".some" with backing 
value of original type.

* Introduction of variable scope (and immutability) with "var" and "let".

* Functions, as well as objects and structs, are now first-class citizens.

* Swift is a very rich language in terms of syntax. With an implicit support for weak variables, lazy loading and concise
semantic keyword meaning a well structured Swift code implies good code readability.  
\enditems

 At the time of development used Swift versions are 4.0 and 4.1, more on Swift in \cite[MasteringSwift-book, OffSwift-book].
 
 
 \sec	Dependency Management

For third party libraries integration I have decided to use dependency management tools. Dependency management tool 
helps keeping specific dependency versions and updating them easily to newer ones. My attention was drawn by two most 
used ones, Carthage and CocoaPods, both of which are very similar, key difference is the method of 
integration.

\secc CocoaPods

CocoaPods is an open-source command line tool highly managed by community \cite[CocoaPods]. Its task is to download 
specified 
dependencies and create a new Xcode (Apple’s IDE for iOS developers) workspace with all linked dependencies. Created
workspace then has to be used for further development. Main drawback of this approach is that all dependencies are linked
directly into project and thus not only application code is compiled, but all dependencies.  
\par This will be used as a secondary dependency management tool for dependencies unavailable with Carthage.


\secc Carthage

Carthage is an open-source command line tool originally developed by Github \cite[Carthage]. Its task is to download 
specific dependency 
versions defined within Cartfile and compile dependency source code into frameworks. User is then responsible for linking 
compiled frameworks into project. This is a preferred approach as it does not require dependencies to recompile each time 
we run application code. To improve compile time user can specify preferred platform (macOS, iOS, tvOS, watchOS) 
dependency code to be compiled for. Dependency contributors may also include pre-compiled binaries, which are then 
downloaded and linked directly into project. Pre-compiled dependencies need be written in same Swift version, however. 
Otherwise, users will not be able to link frameworks into project. This typically happens only when migrating to newer Swift 
version, which in my case was Swift update from 4.0 to 4.1.
\par This will be used as a main dependency manager, largely because of faster build times and more straightforward usage. 


\sec	App architecture, MVVM vs. Apple-MVC

\secc MVC: Model-View-Controller

Since the very first release of iOS in 2007 Apple suggests developers to use a Model-View-Controller (MVC) design pattern
\cite[CocoaMVC].

{\sbf Model } represents objects which define application data-logic. This can be both Data Transfer Objects (DTOs) and 
objects containing logic responsible for data retrieval.  This tier is typically represented by remote service interactor or local 
database access objects.

{\sbf View} contains all the necessary logic for intercepting user interaction and displaying data from {\em Model} tier. It is 
typically composed with hierarchy of other View objects. 

{\sbf Controller} acts as a middleman between {\em View} and {\em Model}. Its job is to process incoming user interaction 
events, manipulate data using {\em Model} and present data to {\em View} to render the final output.


Despite MVC being a common way to organize code for reusability, support extensiveness and decoupling between business 
and UI logic, that is not the case with iOS projects. Apple's definition of MVC uses combined roles variation by 
coupling a View with a Controller class resulting in a so called ViewController, which is then supposed to handle both logic 
for user interactions as well as calls and result delivery from Model tier.

\par The diagram \ref[mvc-diagram] shows the architecture.   

\midinsert \clabel[mvc-diagram]{MVC}
\picw=15cm \cinspic imgs/mvc-diagram.pdf
\caption/f MVC architecture
\endinsert

\par The above implies tight coupling between View and Controller. Mobile applications often contain Views
with complex behavior and animations which have to be managed by Controller, thus resulting in what is often called {\em 
Massive View Controller} \cite[MassVC], which is generally hard to test and mock.


\secc MVVM: Model-View-ViewModel

The MVVM design pattern tries to solve the above drawbacks by introduction of {\em ViewModel} tier. This design 
pattern was originally proposed by Microsoft for .NET frameworks specifically to simplify event-driven programming of user 
interfaces.

{\sbf ViewModel} is an object owned by ViewController, its task is to deal with persistence changes and to ensure data 
retrieval and preparation for View. In other words, ViewModel's responsibility is performing actions initiated by user 
interactions intercepted by ViewController. Possible data updates triggered by such actions are then served again by 
ViewModel to ViewController for display. Architecture behavior can be seen on diagram \ref[mvvm-diagram].

\midinsert \clabel[mvvm-diagram]{MVVM architecture diagram}
\picw=15cm \cinspic imgs/mvvm-diagram.pdf
\caption/f MVVM architecture
\endinsert 


ViewController responsibility is dynamically decreased with introduction of additional ViewModel layer when compared to 
MVC. At first glance, change might not seem like a big advantage, this change however allows easier testing as View layer 
does not have to be created, same behavior can be achieved by calling ViewModel interface methods directly. 
\par Apart from MVC, there is an extra relation. ViewModel does not have reference to ViewController, thus we need to 
specify a mechanism of notifying View layer about data changes. This can be solved using Key-Value-Observable (KVO), a 
standard way supplied with core Apple frameworks. KVO allows to observe data changes of observed objects. Typically, 
ViewController intercepts user events to ViewModel, then gets notified about data change. Unfortunately, this approach 
does not allow synchronization with main thread for view updates. Instead, reactive approach will be used, which is 
explained in the next section.


\sec	Threading and synchronization, motivation for reactive approach

Mobile applications differ a lot from desktop and web alternatives primarily in a variety of input methods. An input 
method can be a device location or even device orientation. One can easily imagine an application driven solely by
accelerometer sensor: a compass application. 
\par For this reason, mobile applications often need to process a lot if incoming events asynchronously to avoid 
main thread overtasking, which may result in irresponsive UI.

\secc Grand Central Dispatch

Provided with iOS SDK, Grand Central Dispatch (GCD) is a standard approach to synchronize background operations 
with main thread. GCD was developed to optimize application support on multi-core 
processors. GCD, backed by a thread pool, manages available system resources for particular installation, 
completely abstracting the underlying manipulation with threads. 
\par To distribute scheduling priority in a "DispatchQueue" instance user supplies an appropriate Quality of Service class
(QoS), see Table \ref[QoS classes] for further explanation.

\midinsert \clabel[QoS classes]{Primary QoS classes summary}
\ctable{lll}{
\hfil QoS class      		 	& Work type 									& Work duration \crl \tskip4pt
 {\sbf User-interactive} 	& Operations on the main thread,  & Instantaneous  \cr
 									& refreshing the user interface, or \cr 
 									& performing animations. 				\cr  
 {\sbf User-initiated} 		& Work, required to continue user & Nearly instantaneous, \cr 
 									& interaction such as opening  		& few seconds or less. \cr 
 									& a photo or a document. 				\cr
 {\sbf Utility}        			& Work that may take some time 	& A few seconds to a  \cr
 									& to complete and doesn’t require & few minutes\cr 
 									& an immediate result, such as 		\cr
 									& downloading or importing data. 	\cr
 {\sbf Background}        	& Work that operates in the background &  Significant time,  \cr
 									& and isn’t visible to the user, such as & minutes or hours. \cr
 									& indexing, synchronizing, and backups. \cr
}
\caption/t  Primary QoS classes summary, taken from \cite[QoS priority].
\endinsert

While being very flexible, dispatch queues only work with first class functions (clojures), which cannot be chained. 
This functionality is available when using "OperationQueue", where single work item is represented with an 
instance of "Operation" subclass. Individual tasks can be prioritized.


\secc Reactive Programming

Reactive programming addresses a propagation of change using data streams. As it is most frequently used to implement 
user interfaces, a typical scenario is to react on user input events. 

{\sbf Functional reactive programming} is paradigm of reactive programming which uses functional programming techniques.
Similar to lists in functional programming, data streams are taken as immutable. If a stream of values needs to be 
transformed, then a new stream must be created. Operators like {\em map}, {\em filter}, {\em flatMap} are also
inspired by functional programming.

There are plenty of example use-cases from UI programming to performing network requests.
For example, one would like to enable or disable a ''Send'' button based on values contained in 3 distinct input fields.
Another example is observing a stream of local database table changes for seamless UI update.

While there are several reactive Swift implementations available, chosen for this project is one from 
ReactiveX\urlnote{http://reactivex.io/} called "RxSwift" \cite[RxSwift]. This implementation of Swift reactive streams
contains a lot of extensions for binding values to UIKit classes properties and has familiar API with another ReactiveX
projects. A more detailed look at ReactiveX implementation of reactive streams in Swift refer to \cite[RxSwift-book]. 
The rules on GCD scheduling priorities imply here as well. 

\par Purpose of this section is not to justify the usage of a particular approach. "RxSwift" is used because it allows
easier data mapping, filtering and general modifications. On the other hand, using "DispatchQueue" may be used
in places where creating custom RxSwift extensions would not make sense due to simplicity of the given task and
easier implementation using standard libraries. 


\sec	View creation and graphic elements
Here I take a closer look at options available for defining user interfaces in Xcode projects. Additionally, I present tooling 
which was used to modify graphic elements.

\secc Interface Builder vs Alternatives
 
A standard way to define user interfaces is the Interface Builder supplied with Xcode. Interface Builder editor allows user to 
compose full view hierarchy with defined navigation behavior between views. All standard ViewControllers which come with 
UIKit framework are supported, as well as creating support for user defined views. The whole project view hierarchy and 
navigation is typically stored within a {\em .storyboard} file. An example editor workspace can be seen on Figure 
\ref[interface-builder].

\midinsert \clabel[interface-builder]{Xcode Interface Builder Example}
\picw=10cm \cinspic imgs/interface-builder.pdf
\caption/f Interface Builder example, source: \url{https://developer.apple.com/xcode/interface-builder/}.
\endinsert

\par One of the most powerful components to define view positioning is Auto Layout\urlnote{https://developer.apple.com/
library/content/documentation/UserExperience/Conceptual/AutolayoutPG/}. Auto Layout is a mechanism of 
constraining UIView object descendant properties to properties of a container window or other UIView descendants.
Interface Builder comes with direct support of Auto Layout, which can alert user when defined constraints result in a conflict.

\par It is worth noting that if a view defined with Auto Layout contains variable child view count, the defined constraints 
should be turned off for proper view stacking. This requires either storing a reference to Interface Builder defined constraint 
object or assembling whole constraints programmatically.  
\par One of the common issues with Interface Builder defined views is that whole project UI definition is contained within a 
single {\em .storyboard} file. A {\em .storyboard} is essentially an XML file, containing almost non-human-readable Interface 
Builder data. This may not seem like a problem at first, however, one should imagine the source control change logs when 
multiple developers are working on a project. It is also worth mentioning that huge {\em .storyboard} files affect view inflation 
speed and build times. 

\par For these reasons I have decided to use an Auto Layout Domain-Specific-Language (DSL) library. A DSL used in this 
project is a relatively popular open-source implementation called SnapKit \cite[SnapKit]. SnapKit will allow us to manage 
dynamic data in single view component easily. A good example is a timeline list with each list item as a reusable view 
component where image and video thumbnail presence may vary.


\secc Graphics

Application theming plays important role in mobile applications. For purposes of this project I have decided to use default 
system font to preserve native user interface look. All used graphics were downloaded freely and then exported using Sketch
\urlnote{https://www.sketchapp.com/}. Sketch is powerful tool for prototyping and designing user interfaces. Additionally, 
can be used to modify imported graphics and export for specific mobile operating system to support different screen 
resolutions. An example Sketch workspace can be seen on figure \ref[sketch-workspace].

\midinsert \clabel[sketch-workspace]{Sketch workspace example}
\picw=10cm \cinspic imgs/sketch-workspace.pdf
\caption/f Sketch workspace with application graphics.
\endinsert


\sec	Functional and Non-Functional requirements

To ensure successful project completion I also had to analyse functional and non-functional requirements. Functional 
requirements are those directly reflected on application functionality. Non-functional requirements are application demands 
which are not necessary related to specific application functions.

\secc Functional requirements

\begitems
* Application can persist authorization state across application relaunch, as well as user profile, first retrieved
making request to user endpoint.
* Application allows user to browse through contest timeline, latest Quest submissions, list of available challenges, Quest 
leaderboards.
* Application allows user to send media submissions. Either image, video or both can be sent with additional text. 
* Every submission contains specific hashtags that uniquely identify contest and Quest challenge.
* Application has support for image display and video playback.
* Application has support for contextual device data acquisition.
* Application allows user to turn on or off device data retrieval.
\enditems

\secc Non-functional requirements

\begitems
* Specific installation refers to specific contest, thus contest needs to be specified before application distribution.
* Application supports OpenID Connect credentials store provided by ICPC.
* Application supports Quest API 1.0 \cite[Filip REST].
* Application user interface is based on present solution.
* Supported iOS versions are 10.3 and above.
\enditems


\vfil \break

\sec	OpenID Connect and Quest REST API 

\secc Quest API
Quest API for mobile clients has support for 
necessary data retrieval and submission. Deployed as a separate module it also manages retrieved contextual device
data for later research. This module also handles verification of all incoming request using authorization framework.

\secc OpenID Connect
For security reasons every request made on Quest back-end must be authorized. This is achieved using an authorization 
framework, which verifies user identity using uniquely assigned token.
OpenID Connect (OIDC)\urlnote{http://openid.net/connect/} is an authentication layer on top of OAuth 2.0 authorization
framework. From a mobile developer perspective all needed to be taken care of is:

\begitems
* Authorization endpoint
* Token exchange endpoint
* Registered client id
* Redirect URL
* Logout endpoint
\enditems

For the purpose of this project "AppAuth" is used, a library that implements OIDC specification \cite[AppAuth]. This library 
follows modern practices for performing authorization requests in native clients, such as displaying request web page in 
{\em SFSafariViewController} instead of {\em UIWebView} \cite[OAuthRFC]. Also, a big advantage of this library is 
automatic token exchange, meaning user only needs to react on authorization state changes.

\sec Motivation behind Context Acquisition

Context acquisition is motivated by several studies addressing authentication security and identity 
verification by utilizing surrounding WiFi and BLE enabled devices. Article \cite[Article A] discusses an 
approach to user authentication process by capturing user activities and behavioral patterns, which can be
uniquely represented by surrounding WiFi-enabled devices meta-data.  In \cite[Article B] location-based 
authentication approach is described using information from nearby IoT devices. Potential treats of 
surrounding devices and services intervention is described in \cite[Article C]. With respect to Apple restrictions 
and limitations of available API the most promising approach might be to make extensive use of surrounding 
devices conforming to Apple-developed iBeacon protocol as described in \cite[Article B].

The purpose of context acquisition feature is to introduce a prototype of an iOS application capable of
collecting surrounding device information and sending such data to MyICPC for analysis.
Context acquisition is implemented with respect to data predefined and expected by MyICPC REST API
for mobile clients \cite[Filip REST].

\sec	Context Acquisition and Restrictions

For the purpose of future ICPC research Quest client has to support functional requirement on context data acquisition. 

\par Contextual data are defined as:
\begitems
* information about the device;
* device location;
* connected network;
* available networks;
* local network devices;
* Bluetooth devices;
\enditems

Unfortunately, not everything is accessible, as Apple has always been strict about its security policy. Some of the data 
listed above may be gained using private libraries, these are intended to be used by Apple applications only. Given
that the most convenient way to distribute this project would be the App Store, the only officially supported way, some 
of the requested data are ignored.

Next sections describe whether information can be retrieved.

\secc Device-specific information
\begitems
* source
* timestamp
* operating system
* device brand
* device model
* device serial number
\enditems

Source indicates application the context is being sent from. Timestamp can be easily gained using Foundation framework 
classes, the later correspond to information easily retrievable using "UIDevice" class from UIKit framework 
\cite[UIDevice appledoc].

\secc Device location
Device location should include:
\begitems
* latitude
* longitude
* timestamp
\enditems

All of the listed above is easily retrievable using Core Location framework \cite[Core Location appledoc]. A user permission 
needs to be granted. 

\secc Connected network
Information about connected network:
\begitems
* SSID
* BSSID
* RSSI
* link speed
* frequency
* IP address
\enditems

This requires looping through supported system interfaces and using SystemConfiguration 
framework to copy interface data using "CNCopyCurrentNetworkInfo" \cite[SystemConfiguration appledoc].
However, only SSID and BSSID are accessible this way. There is an option to request entitlements and use 
"NEHotspotHelper" class, which is discussed in next subsection.


\secc Available networks

This requirement violates Apple security policy. This functionality is primarily accessible to Apple applications only as it 
requires either private library or special entitlements. Entitlements need to be requested for NetworkExtension
framework, specifically one would need to use "NEHotspotHelper" class, this option requires paid Apple Developer 
account \cite[NetworkExtension appledoc].


\secc Local network devices
Network device should contain:
\begitems
* IP address
* MAC address
\enditems
This task requires a classic network scanner which will ping every host available in network in order to build the ARP table. 
ARP table is then used to obtain MAC address and a host name. For this purpose an open-source library is used 
\cite[MMLanScan]. It should be taken into account that MAC address and host name retrieval will only work on devices 
running iOS 10.3 and below, as Apple restricts this functionality in iOS 11 due to API misuse, which enabled mobile clients to 
track users \cite[MAC addr iOS 11].


\secc Bluetooth devices
Bluetooth device is supposed to contain:
\begitems
* name
* MAC address
\enditems
Apple devices running iOS support only discovery of Bluetooth Low-Energy (BLE) devices and only those, transmitting 
limited number of services \cite[Core Bluetooth appledoc]. Core Bluetooth framework does not guarantee proper name 
retrieval. A specific device name can be retrieved after device pairing, this may, however, drain device battery significantly.
MAC address is unavailable, Core Bluetooth assigns specific device a UUID number, which is then used as unique device 
identifier.
  
