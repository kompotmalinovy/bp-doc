\chap Implementation

For more detailed look at implementation refer to application sources at \cite[app-source].
\sec Project Environment

Project is set up to support 2 schemes: Development and Production. Each scheme has its own property list,
a {\em .plist} file. A proporty list defines all application permissions as well as scheme-specific constants.
In my case, these are: contest id, Authentication endpoints, API base URL, contest rules link and application
version for context submissions. A convenience enum is declared as shown:

\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
public enum Environment {
    
    fileprivate static let plist: [String: Any] = Bundle.main.infoDictionary!
    
    enum Contest {
        static var code: String { return plist["contestCode"] as! String }
    }
    
    enum Auth {
        fileprivate static let authDict = plist["auth"] as! [String: Any]
        
        static var authEndpoint: String { return authDict["AuthEndpoint"] as! String}
        /* other OIDC settings */
    }
    
    enum API {
        fileprivate static let apiDict = plist["api"] as! [String: Any]
        
        static var baseURL: String { return apiDict["BaseUrl"] as! String }
    }
    
    enum External {
        fileprivate static let linksDict = plist["external"] as! [String: Any]
        
        static var rulesLink: String { return linksDict["RulesLink"] as! String }
    }
    
    enum Context {
        static var appVersion: String { return plist["appVersion"] as! String }
    }
}

\endtt

Next, project is preconfigured to support different localizations, the base one is English. To simplify work with 
localized strings, assets and colors I have opted to use SwiftGen which generates convenient enums based on 
resource declaration names \cite[SwiftGen].

\par With the dependency managers installed and build target specified, project is configured for 
development.

\sec Application Design and Navigation use-cases

Having project set up, I have started designing a navigation map with screen prototypes. 
One of the functional requirements is to display 4 distinctive information types (timeline, recent 
submissions, challenges, leaderboards). For this reason, 4 views should be accessible form the top 
application level. To achieve such view composition, a "TabBarController" is used, as suggested by 
Apple Design Guidelines \cite[TabBarGuidelines]. Implementation of specific screens is described
in later sections.

\par Part of the navigation map for challenge list tab can be seen on Figure \ref[navigation-map].
\midinsert \clabel[navigation-map]{Navigation map for challenge list tab}
\picw=15cm \cinspic imgs/navigation-map.pdf
\caption/f Navigation map for challenge list tab.
\endinsert 

\sec User managment, Keychain managment and Application settings

Components necessary for application state management (including state persistence across application 
relaunch) are decomposed to several {\em Manager} classes.

{\sbf AppSettingsManager} is a manager responsible for persisting application settings valid for particular 
installation. Later, we would like to display a dialog asking user for a permission to send contextual data 
on the very first application start. We will simply store a "Bool" property indicating whether permission 
was requested as well as whether context data fetch is allowed. These properties will be stored in 
"UserDefaults", a transactional key-value store for saving application small data.

{\sbf KeychainManager} is an abstraction over generic wrapper \cite[Locksmith] performing read/write 
operations in Keychain, a secure storage to persist application sensitive data, such as current 
authorization state with latest tokens. Data stored in Keychain are safely persisted even after application 
uninstall, {\em AppSettingsManager} is then used to check whether it is a very first run of a particular 
installation, appropriate action will then be taken: either delete everything on first run or use stored data 
for token exchange.

{\sbf UserManager} is a wrapper around {\em AppAuth} and abstracts token exchange and authentication 
requests. Instance of this class is then provided as a dependency to other application components, as I 
demonstrate in section {\sbf Dependency Injection} [ref].


\sec API description and interactor

All available networking operations are defined within "ApiDescription" class. To perform API requests
a popular HTTP networking library is used called {\em Alamofire} with community extensions for RxSwift
\cite[Alamofire, AlamofireImage, RxAlamofire]. 
"ApiDescription" class contains definition of all used endpoints and specific return type mapping. 
Following example shows endpoint for fetching timeline items.

\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
func getTimeline(contestCode: String, token: String) -> Observable<[Notification]> {
        return RxAlamofire.requestData(
                            .get,
                            Environment.API.baseURL + "timeline/\(contestCode)",
                            headers: ["Authorization": "Bearer \(token)"]
                )
                .map(to: [Notification].self)
}
\endtt

"ApiInteractor" class is then defined to tie "ApiDescription" with "UserManager" and perform authorized 
API requests. Following the reactive programming principles we observe a stream of exchanged tokens, 
which is a single emission, and another stream, based on original token stream, is created to perform the 
actual HTTP request to server and emit a value of a mapped type.

\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
func getTimeline(contestCode: String) -> Observable<[Notification]> {
        return userManager.observeFreshToken()
                .flatMap { token in
                    self.apiDescription.getTimeline(contestCode: code, token: token)
                }
}
\endtt
 
\sec Repository Pattern

REST APIs are not always the only data source in mobile applications. A data source can be represented 
as a local storage which can be used to cache data for offline usage. Despite having single source, I 
mask data retrieval using Repository pattern\urlnote{https://martinfowler.com/eaaCatalog/repository.html}.

\par Regardless of having single data source, here I am showing another benefit of using Repository on a 
very simple example. 
\par Incoming data are not always defined in a way suitable for presenting in View layer, sometimes 
developer may want to alter incoming objects. Repositories are a perfect place for the job. One of the 
functional requirements is to display leaderboard types and a specific leaderboard rows. API defines an 
endpoint for retrieving dynamic list of leaderboard types and en endpoint for retrieving leaderboard rows 
with points \cite[Filip REST]. However, there is also a separate endpoint to fetch team leaderboard rows, 
which is always available (does not have to be permitted by MyICPC administrator). Does it require us to 
adopt View layer to access same data is different ways? Well, turns out no. Instead, we use "LeaderboardRepository" to transform data to be later displayed in appropriate way.

\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
enum LeaderboardType {
    case generic(Leaderboard)
    case team
}

class LeaderboardRepository: LeaderboardRepositoring {
    private let apiInteractor: ApiInteracting
    
    init(apiInteractor: ApiInteracting) {
        self.apiInteractor = apiInteractor
    }
    
    func observeLeaderboards() -> Observable<[LeaderboardType]> {
        return apiInteractor.getLeaderboardTypes()
            .map { $0.map { LeaderboardType.generic($0)} + [LeaderboardType.team] }
    }
}
\endtt

This way, all available leaderboard types are displayed within a single list, no other special view needs to 
be created to navigate to team leaderboard. For future leaderboard rows loading, again a 
"LeaderboardType" enum is used to distinguish a requested type and which endpoint to use.

Following the single responsibility principle, each of "Repository" object will be responsible for particular
resource type: "Notification", "Leaderbord", "Challenge" and "Context".


\sec Context Repository

In this section I describe the implementation of application's key feature: context acquisition.
This Quest version is designed to fetch device and network data on every user submission. This way, 
context fetch is triggered after verifying submission is not empty. Network and BLE devices are then 
scanned for specified time interval.

Fetching different data types asynchronously and combining results may seem like a troublesome task. 
However, with reactive approach it is a matter of couple lines of code, the important thing is a concept, 
which I shall demonstrate next for each context data type.


\secc App source, Timestamp, Device info, Connected Network Info

These are the easiest context types to retrieve, as they do not require heavy logic to produce the output.
For each type I create a stream emitting a single entity.
\par An example is shown for App source:

\begtt
private func observeAppSource() -> Observable<String> {
    return Observable.just(Environment.Context.appVersion)
}
\endtt

\secc Device location
To obtain device location {\em CoreLocation} framework is used with open-source reactive extensions on 
"CLLocationManager" class \cite[RxCoreLocation]. A separate "Provider" class is used to allow future
system sensor functionality mock in unit tests.
%Location retrieval has to be accepted by user.

\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
class RxLocationProvider: RxLocationProviding {
    private let locationManager = CLLocationManager()

    func observeLocation() -> Observable<Location?> {
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        return locationManager.rx.location
            .take(1)
            .map { $0?.toLocation() }
            .do(onDispose: { self.locationManager.stopUpdatingLocation() })
    }
}
\endtt


\secc Network devices
{\em MMLanScan} library is used to scan local network devices \cite[MMLanScan]. "RxLanScanner" class 
abstracts library functionality and provides a stream emitting every found device. An instance of "PublishSubject" is used to act both as an "Observer" and a cold "Observable" (emits only objects available after subscribing to the stream).

\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
class RxLanScanner: NSObject, MMLANScannerDelegate, RxLanScanning {
    private let deviceSubject: PublishSubject<MMDevice> = PublishSubject<MMDevice>()
    private var scanner: MMLANScanner!

    override init() {
        super.init()
        scanner = MMLANScanner(delegate: self)
    }

    func lanScanDidFindNewDevice(_ device: MMDevice!) {
        deviceSubject.on(.next(device))
    }

    func lanScanDidFinishScanning(with status: MMLanScannerStatus) {
        deviceSubject.on(.completed)
    }

    func lanScanDidFailedToScan() {
        deviceSubject.on(.error(LanScanError.failedToScan))
    }

    // MARK: RxLanScanning
    func observeLANDevices() -> Observable<MMDevice> {
        return deviceSubject.asObservable()
                .do(onSubscribe: { self.scanner.start() })
                .do(onDispose: { self.scanner.stop() })
    }
}
\endtt
With a stream of found devices, we would now want to satisfy a requirement to scan for certain 
amount of time. Plus, we need to create a new stream which will emit a {\em list} of all found devices,
all this is easily achieved using reactive operators.

\begitems \style n
* all emissions are mapped to defined DTO object accepted by Quest back end.
* using {\em take(duration:, scheduler:)} a time interval is specified, as well as scheduler timeout event 
is to be triggered from. Stream is completed on timeout.
* all emissions are mapped to a single emission of "LANDevice" array.
* if "RxLanScanner" emits an error, a stream of single empty array is returned.
\enditems

\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
class RxLANDeviceProviderImpl: RxLANDeviceProvider {
    private let lanScanner: RxLanScanning = RxLanScanner()
    
    func observeLANDevices(timeInterval: RxTimeInterval) -> Observable<[LANDevice]> {
        return lanScanner.observeLANDevices()
/* 1 */    .map { LANDevice(ipAddress: $0.ipAddress, macAddress: $0.macAddress)}
/* 2 */    .take(
                timeInterval,
                scheduler: ConcurrentDispatchQueueScheduler(qos: .utility)
            )
/* 3 */    .toArray()
/* 4 */    .catchErrorJustReturn([LANDevice]())
    }
}
\endtt

\secc BLE devices

Scan for BLE devices is performed using {\em CoreBluetooth} framework and an open-source reactive 
abstraction \cite[Core Bluetooth appledoc, RxBluetoothKit]. Again, strategy is same as for scanning 
network devices. First, observe the state of "CentralManager" of {\em CoreBluetooth} framework. Once 
Bluetooth sensor is powered on, we shall start the actual device scan. Device scan is performed within 
duration of specified time interval and then transformed into a stream of device array. Passing "nil" as
parameter of {\em scanForPeripherals(withServices:)} indicates that scan should be performed for all 
supported BLE services. Empty array is returned on error.

\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
class RxBLEDeviceProviderImpl: RxBLEDeviceProvider {
    private let centralManager = CentralManager(queue: .global())
    
    func observeBLEDevices(timeInterval: RxTimeInterval) -> Observable<[BTDevice]> {
        return centralManager.observeState()
            .startWith(centralManager.state)
            .filter { $0 == .poweredOn }
            .timeout(
                timeInterval,
                scheduler: ConcurrentDispatchQueueScheduler(qos: .utility)
            )
            .take(1)
            .flatMap { _ in
                self.centralManager.scanForPeripherals(withServices: nil)
                    .map { BTDevice(name: $0.peripheral.name) }
                    .take(
                        timeInterval,
                        scheduler: ConcurrentDispatchQueueScheduler(qos: .utility)
                    )
                    .toArray()
            }
            .catchErrorJustReturn([BTDevice]())
    }
}
\endtt


\secc Pack it all together
With streams for each individual context item, it is time to provide a mechanism blending all emissions 
into a "Context" DTO which is then sent to MyICPC. This is again done easily using reactive operator
{\em combineLatest(source:, combiner:)} which combines latest emissions from all supplied streams and produces a stream of entities composed in {\em combiner} closure.

\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
func observeContext() -> Observable<Context?> {
    guard appSettings.contextPermissionGranted else {
        return Observable.just(nil)
    }
    
    return Observable.combineLatest(
        observeAppSource(),
        observeTimestamp(),
        locationProvider.observeLocation(),
        deviceProvider.observeDevice(),
        networkProvider.observeConnectedNetwork(),
        bleDeviceProvider.observeBLEDevices(timeInterval: Values.bleScanTimeout),
        lanDeviceProvider.observeLANDevices(timeInterval: Values.lanScanTimeout)
    ) { source, timestamp, location , device, connectedNetwork,
        bleDevices, lanDevices in
        Context(
            source: source,
            timestamp: timestamp,
            device: device,
            location: location,
            connectedNetwork: connectedNetwork,
            bleDevices: bleDevices,
            lanDevices: lanDevices
        )
    }
}
\endtt

We are now ready to observe context emissions and provide data to MyICPC. An example of data
fetched during development is shown in Table \ref[ContextData].
\midinsert \clabel[ContextData]{Example context data sent to MyICPC}
\ctable{lll}{
\hfil Type      		 		& Data 													 							\crl \tskip2pt
{\sbf Source}				&	"MyICPC Quest 1.0 dev (iOS)"			 							\cr
{\sbf Timestamp} 		& 2018-04-29 21:11:16 +0000  	 			 							\cr
{\sbf Device} 			& operatingSystem: "iOS",  version: "11.2.6", \cr
								& deviceBrand: "Apple", \cr
								& deviceModel: "Vlad Gorbunov's iPhone",  \cr
								& serial: Optional("1217A859-0D53-43A1-BBA5-F6C4B7246C6D") \cr
{\sbf Location}			& Optional(timestamp: 2018-04-29 21:11:02 +0000, \cr 
								& latitude: 50.053771412977994, longitude: 14.42745745183381) \cr
{\sbf Connected Network} & ssid: Optional("BOHEMIA"), bssid: Optional("8:60:6e:5f:6a:44")  \cr
{\sbf BLE devices} 	& name: Optional("Vlad’s MacBook Pro") \cr
								& name: Optional("Vlad’s iPad") \cr
								& name: Optional("JBL Flip") \cr
{\sbf LAN devices}	& ipAddress: "192.168.1.1", macAddress: "02:00:00:00:00:00" \cr
								& ipAddress: "192.168.1.22", macAddress: "02:00:00:00:00:00" \cr
								& ipAddress: "192.168.1.87", macAddress: "02:00:00:00:00:00" \cr
}
\caption/t  Example of possible context submission.
\endinsert


\sec A closer look at ViewModels with RxSwift

With data sources set up, it is time to tie a ViewController with ViewModel and describe data loading 
state propagation. For the sake of simplicity, an example is shown for user profile view.

\par To represent loading operation states a generic enum with associated values is used:
\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
enum State<T> {
    case idle
    case empty
    case error(Error)
    case loading
    case loaded(T)
    case reloading
}
\endtt


\par "ProfileViewModel", in its simplified form, contains RxSwift "Variable" property producing stream of 
resource loading states and a computed property indicating whether context acquisition is 
allowed by user. During initialization step, occurance of stored "User" object is checked in application 
storage and passed immediately to {\em userVariable} if present, otherwise requested from MyICPC. 
Stream of "User" loading operation states is exposed by {\em observeUser()} method.

\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
class ProfileViewModel: ViewModel {
    private let userVariable = Variable(State<User>.idle)
    /* -- omitted private properties -- */
      
    init(/* -- ViewModel dependencies -- */) {
        /* -- omitted sets -- */     
        if let user = userManager.user {
            userVariable.value = .loaded(user)
        } else {
            loadUser()
        }
    }

    var isContextSettingEnabled: Bool {
        get { return appSettings.contextPermissionGranted }
        set { appSettings.contextPermissionGranted = newValue }
    }
    
    func loadUser() {
        userVariable.value = .loading
        repository.observeUser()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
            .subscribe { event in
                switch event {
                case .next(let user):
                    self.userManager.user = user
                    self.userVariable.value = .loaded(user)
                case .error(let error):
                    self.userVariable.value = .error(error)
                }
            }
            .disposed(by: disposeBag)
    }
    
    func logout() {
        userManager.logout()
    }
    
    func observeUser() -> Observable<State<User>> {
        return userVariable.asObservable()
    }
}
\endtt

ViewModel is provided to ViewController using composition. Once ViewController descendant instance
is created, it needs to be passed for presentation. Once passed for presentation, there are several
lifecycle events needed to be reacted on. First, a {\em loadView()} method is called. Here we declare
child view components and specify how they should be layouted. Next, we request data, prepare UI
bindings and subscribe for data load states, all in {\em viewDidLoad} method. This method is called
only once when ViewController's {\em view} property is lazily initialized. Once ViewController's {\em view}
is deattached from "UIWindow" container, it and all its properties are deinitialized (unless strong 
reference to ViewController exists). Reactive stream subscriptions must be disposed to prevent 
potential memory leaks caused by reference cycles. In case of RxSwift, we provide a reference to a 
"DisposeBag" which disposes all subscriptions on deinitialization.

\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
class ProfileViewController: UIViewController {
    // omitted private properties
    
    init(viewModel: ProfileViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    // MARK: lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.observeUser()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { state in
                	// display user and load profile pic
            })
            .disposed(by: disposeBag)
        
        contextOnSwitch.setOn(viewModel.isContextSettingEnabled, animated: false)
        
        contextOnSwitch.rx.value
            .skip(1)
            .subscribe(onNext: { value in
                self.viewModel.isContextSettingEnabled = value
            })
            .disposed(by: disposeBag)
    }
    
    
    // MARK: private methods    
    @objc private func didSelectRules() {
        let controller = SFSafariViewController(url:
            URL(string: Environment.External.rulesLink)!)
        navigationController?.present(controller, animated: true)
    }
    
    // MARK: view definition
    override func loadView() {
        super.loadView()
        navigationItem.title = L10n.Navigation.profile
        // -- omitted view creation code --
    }
}
\endtt

\vfil \break

\sec Assembling pieces, Dependency Injection and Coordinators

\secc Dependency Injection

To prevent singletons and allow easier maintenance I have decided to adopt a concept of Dependency 
Injection\urlnote{https://www.martinfowler.com/articles/injection.html}. The DI container in this project is
represented by Swinject, a DI framework for Swift \cite[Swinject].
In the below explanation I am using terminology provided by Swinject documentation\urlnote{https://github.com/Swinject/Swinject/blob/master/Documentation/DIContainer.md}:
\begitems
* {\sbf Service}: A protocol defining an interface for a dependent type.
* {\sbf Component}: An actual type implementing a service.
* {\sbf Factory}: A function or closure instantiating a component.
* {\sbf Container}: A collection of component instances.
\enditems

First, we start by registering a {\em Service}. Service is registered by providing a type of a defining 
"protocol" and providing protocol conforming implementation inside factory closure.
\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
class AppContainer {
    
    static let container = Container() { container in

        container.register(KeychainManaging.self) { _ in
            KeychainManager()
        }.inObjectScope(.container)    
        
        /* -- other services --*/
    
        container.register(UserManaging.self) { resolver in
            UserManager(
                keychainManager: resolver.resolve(KeychainManaging.self)!,
                settingsManager: resolver.resolve(AppSettingsManaging.self)!
            )
        }.inObjectScope(.container)
        
        container.register(ApiServicing.self) { resolver in
            ApiInteractor(
                userManager: resolver.resolve(UserManaging.self)!,
                apiDescription: ApiDescription()
            )
        }.inObjectScope(.container)
        
        /* -- other services -- */
        
        container.register(ChallengeListViewModel.self) { r in
           ChallengeListViewModel(repository: r.resolve(ChallengeRepositoring.self)!)
        }
    }
}
\endtt

In the above example, concrete implementation may have its own dependencies, these have to be 
resolved using a "Resolver" provided by {\em .register(Service.Type, factory: (Resolver, args..))} method.
Swinject then finds registered Services of requested type in dependency graph and provides an instance 
valid in assigned object scope. By specifying {\em .inObjectScope(.container)} we are telling Swinject to 
use a single instance for current container, as well as for child containers.

\par The tricky part comes when we need to assemble ViewController nested dependencies before 
presentation. Since I have opted not to use Storyboards for view creation and navigation, all navigation 
logic is handled manually. In the above example we assume every {\em Service} is provided using a {\em 
Component} -- the actual implementation of a service. Suppose we have a ViewController displaying list 
of items and we want to display item detail by presenting an item detail ViewController. Supplying 
instance of detail ViewController will not do the trick, as it will require us to use some sort of method 
injection to specify list item we want to display and, based on lifecycle methods, we would then reload 
view for the up-to-date data. This is conceptually wrong as MVVM architecture suggests that displayed 
data should be owned by ViewModel. Instead, we provide a factory for the detail ViewController 
which will assemble all necessary dependencies managed within Swinject Container. 
In the below example, a specific challenge is passed as a factory argument and is then used to resolve 
"ChallengeViewModel" with underlying dependencies. 

\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
typealias ChallengeViewControllerFactory =
    (_ coordinator: ChallengeCoordinator, _ challenge: Challenge)
    -> ChallengeViewController


container.register(ChallengeViewControllerFactory.self) { r in
    return { coordinator, challenge in
        let controller = ChallengeViewController(
            viewModel: r.resolve(ChallengeViewModel.self, arguments: challenge)!
        )
        controller.navigationDelegate = coordinator
        return controller
    }
}
\endtt


\secc Coordinators

Coordinators are special types of controllers which mask navigation controllers ("UINavigationController", "UITabBarController", ...)\urlnote{https://developer.apple.com/library/content/documentation/WindowsViews/Conceptual/ViewControllerCatalog/Chapters/NavigationControllers.html} 
functionality.

It is an iOS-specific adoption of an {\em Application Controller}. In {\em Patterns of Enterprise Application Architecture} it is described as a {\em ''centralized point for handling screen navigation and the flow of 
application''} \cite[PEAA], page 379. 
Coordinator's key responsibility is to allow easier navigation flow and perform tasks too general for the 
scope of particular ViewController. Navigation logic is delegated to responsible Coordinator by 
Coordinator conformation to protocols supported by ViewControllers, which are being ''coordinated''.
Combination of this pattern with MVVM is often referred to as {\sbf MVVM-C}\urlnote{https://tech.trivago.com/2016/08/26/mvvm-c-a-simple-way-to-navigate/}, however, with emerging
cross-app functionality and adoption of deep linking in mobile applications, Coordinators have become 
an essential part of a good MVVM implementation. 

\par In my implementation each coordinator is assembled with provided ViewController factories and is 
backed by supported UIKit navigation controller. Once navigation flow is started, Coordinator is supplied 
to ViewController before presentation using property injection. Coordinator is then stored as a weak 
reference to prevent reference cycle.

\par An example is shown for "AppCoordinator", responsible for authorization state flow and managing
top level "UITabBarController".

\vfil \break

\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
// AppCoordinator is started once application finished launching
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(/* -- omitted params -- */) -> Bool {
        let appCoordinator = AppContainer.container.resolve(
                AppCoordinator.self, argument: UIWindow(frame: UIScreen.main.bounds)
            )!
            
        appCoordinator.start()
        return true
    }
}

final class AppCoordinator: NSObject {
    
    init(/* -- provided factories -- */) {
    }
    
    func start() {
        window.makeKeyAndVisible()
        if userManager.isAuthorized {
            showMain()
        } else {
            window.rootViewController = loginFactory(self)
        }
        
        userManager.observeAuthChanges()
            // -- subscribe to auth changes to switch main view to login view
            .disposed(by: disposeBag)
    }
    
    private func showLogin() {
        window.rootViewController = loginFactory(self)
    }
    
    private func showMain() {
        /*
            init tabBarController with child coordinators
        */
        tabbarController.viewControllers = [timelineCoordinator.rootViewController,
                                        feedCoordinator.rootViewController,
                                        questsCoordinator.rootViewController,
                                        leaderboardCoordinator.rootViewController]
        
        window.rootViewController = tabbarController
        
        // check if context dialog was shown
        if !appSettings.contextPermissionRequested {
            // show dialog asking to send context for research
        }
    }
}

// confirm to protocol accepted by LoginViewController
extension AppCoordinator: LoginNavigationDelegate {
    
    func didSelectLogin() {
        // perform auth request
    }
}
\endtt


\vfil \break

\sec Notifications and Challenges

Notifications list is implemented using "UITableViewController" with support for different 
"UITableViewCell" types: "NotificationCell" and "SystemNotificationCell". It is a generic view used with 
several display modes:
\begitems
* {\sbf timeline} -- all notifications produced by users and system notifications of MyICPC;
* {\sbf whatsHappeningNow} -- latest Quest submissions;
* {\sbf accepted(Challenge)} -- accepted Quest submissions for particular challenge;
* {\sbf pending(Challenge)} -- pending submissions for specified challenge;
* {\sbf rejected(Challenge)} -- list of rejected Quest submissions for challenge.
\enditems

An important thing here worth pointing out is that both "NotificationCell" and "SystemNotificationCell" 
have a variable height due to multiline text, image and video presentation. This may result in a UI glitch 
on scroll, unless correctly implemented.
\par To prevent layouting problems, a property of "UITableView" indicating variable row height has to be set with accurate estimate.

\begtt
tableView.rowHeight = UITableViewAutomaticDimension
// estimate is computed using defined cell insets and constraints
tableView.estimatedRowHeight = 300
\endtt

\par UIKit does not provide an automatic mechanism for hiding views and switching off its constraints. 
This behavior is only available with "UIStackView". "UIStackView" computes child view positioning 
attributes during the actual drawing, thus it is not applicable in our case. Instead, we need to switch 
constrains on and off and hide redundant views manually. This is where AutoLayout DSL comes into play. 
With SnapKit we can easily remake existing constraints to adopt a cell for specific data.

\par Each cell instance is a reusable view object contained withing the {\em object pool} it was registered 
in. To minimize possible glitch and achieve maximum efficiency I am registering each 
possible cell type data configuration with its own object pool.

\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt// register a cell configuration in a specific object pool
tableView.register(NotificationCell.self, 
        forCellReuseIdentifier: NotificationCell.Identifiers.withText)

// deque cell later using identifier
tableView.dequeueReusableCell(withIdentifier: NotificationCell.Identifiers.withText, 
        for: indexPath)
\endtt

\par Notification image and video thumbnails are expanded on click for presentation using 
"SKPhotoBrowser" \cite[PhotoBrowser] and "AVPLayerViewController" from standard {\em AVFoundation} framework.

\par Same rules to achieve smooth scrolling are applied in challenge list view.

\par Final design of challenge and notification lists can be seen on Figures \ref[gallery-notifications] \ref[gallery-challenges].
\par Application also comes with user-friendly error and empty states as depicted on Figure \ref[gallery-errors]


\midinsert \clabel[gallery-notifications]{Notification list design}
\picw=15cm \cinspic imgs/gallery-notifications-v2.pdf
\caption/f  Final design of notification list.
\endinsert


\midinsert \clabel[gallery-challenges]{Challenge list design}
\picw=15cm \cinspic imgs/gallery-challenges.pdf
\caption/f  Final design of challenge list and challenge detail.
\endinsert

\midinsert \clabel[gallery-errors]{Error state mapping design}
\picw=15cm \cinspic imgs/gallery-errors.pdf
\caption/f  Error state mapping.
\endinsert

\vfil \break

\sec Leaderboards

Original "LeaderboardRowResponse" entity comes from API with 2 properties: name and points. To make 
an overall leaderboard appearance more attractive, a maximum of provided row points is found and is 
used to transform original row list into new "LeaderboardRow" entities containing
additional {\em maxPoints} field. This property is used later to display a horizontal bar chart indicating 
current progress using Charts library \cite[Charts]. 
\par Final leaderboards design can be seen on Figure \ref[gallery-leaderboard].

\midinsert \clabel[gallery-leaderboard]{Leaderboard list design}
\picw=15cm \cinspic imgs/gallery-leaderboard.pdf
\caption/f  Final design of leaderboard list.
\endinsert


\sec Input

Input view is a crucial part of application. As in every other app which is aimed at data retrieval, the 
better input method we provide, the better input data we get. In my case input view is presented as an
"UITextView" for multiline text and a "UICollectionView" for horizontal collection of picked images and 
videos. "InputViewController" is designed to support any number of picked media files and in this version 
is explicitly restricted to exactly one of each media file type. There I also observe a keyboard 
opening event using KVO to adjust positioning of bottom bar from which image and video picker is 
accessed. Media picker used in this project is an open-source library called {\em Gallery} \cite[Gallery].

Interesting part here is also how user inputs are observed to enable/disable ''Send'' button using reactive extensions which come with {\em RxCocoa} (provided as part of RxSwift).

\bgroup\addto\tthook{\aftergroup\egroup\typosize[9/11]}
\begtt
let isTextPresent = viewModel.message.asObservable()
    .map { !$0.isEmpty && self.messageTextView.textColor != self.textViewHintColor }
let isMediaPresent = viewModel.observeMedia()
    .map { !$0.isEmpty }
let isLoading = viewModel.observeInputState()
    .map { $0 == .uploading }
let isPostEnabled = Observable.combineLatest(isTextPresent,
                                             isMediaPresent,
                                             isLoading) {
    ($0 || $1) && !$2
}

isPostEnabled.bind(to: navigationItem.rightBarButtonItem!.rx.isEnabled)
    .disposed(by: disposeBag)
\endtt

\par Same strategy is applied to observe media files submission limits and enable/disable media picker 
buttons.
\par Final input view design can be seen on Figure \ref[gallery-input].

\midinsert \clabel[gallery-input]{Input view design}
\picw=15cm \cinspic imgs/gallery-input-v1.pdf
\caption/f  Final input view design.
\endinsert

\vfil \break

\sec Other views

Here I present some views which are not in scope of previous sections. 
\par There is nothing particularly 
interesting that would be worth pointing out. Final designs are presented for completeness and can be 
seen on Figure \ref[gallery-login-profile].

\midinsert \clabel[gallery-login-profile]{Login and Profile views design}
\picw=15cm \cinspic imgs/gallery-login-profile.pdf
\caption/f  Final design of Login and Profile views.
\endinsert